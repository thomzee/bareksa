<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->uuid('id')->unique()->primary();
            $table->string('title', 100);
            $table->longText('content')->nullable();
            $table->string('status')->default('draft');
            $table->uuid('topic_id');
            $table->string('topic_name');

            $table->foreign('topic_id')->on('topics')->references('id')->onDelete('cascade');
            $table->timestamps();

            $table->index([
                'title',
                'status',
                'topic_name'
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
