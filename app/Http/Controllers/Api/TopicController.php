<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Topic\CreateRequest;
use App\Http\Requests\Api\Topic\UpdateRequest;
use App\Mappers\TopicMapper;
use App\Repositories\TopicRepository;
use App\Services\Mapper\Facades\Mapper;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class TopicController extends Controller
{
    protected $topic;

    public function __construct(TopicRepository $topic)
    {
        $this->topic = $topic;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        try {
            $data = $this->topic->paged($request);
            return Mapper::list(new TopicMapper(), $data, $this->topic->count(), $request->method());
        } catch (\Exception $e) {
            return Mapper::error($e->getMessage(), $request->method());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateRequest $request
     * @return Response
     */
    public function store(CreateRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = $this->topic->create($request);
            DB::commit();
            return Mapper::single(new TopicMapper(), $data, $request->method());
        } catch (\Exception $e) {
            DB::rollBack();
            return Mapper::error($e->getMessage(), $request->method());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function show(Request $request, $id)
    {
        try {
            $data = $this->topic->find($id);
            return Mapper::single(new TopicMapper(), $data, $request->method());
        } catch (\Exception $e) {
            return Mapper::error($e->getMessage(), $request->method());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param int $id
     * @return Response
     */
    public function update(UpdateRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $data = $this->topic->find($id);
            $data = $this->topic->update($data, $request);
            DB::commit();
            return Mapper::single(new TopicMapper(), $data, $request->method());
        } catch (\Exception $e) {
            DB::rollBack();
            return Mapper::error($e->getMessage(), $request->method());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function destroy(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $data = $this->topic->find($id);
            $this->topic->delete($data);
            DB::commit();
            return Mapper::single(new TopicMapper(), $data, $request->method());
        } catch (\Exception $e) {
            DB::rollBack();
            return Mapper::error($e->getMessage(), $request->method());
        }
    }
}
