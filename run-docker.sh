#!/usr/bin/env bash
rm .env
cp .env.docker .env
docker-compose up -d
docker-compose exec app composer install
docker-compose exec app php artisan config:cache
docker-compose exec app php artisan migrate
