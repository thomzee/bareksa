## Introduction
This repository contains test assessment given by Bareksa. This project done with PHP and Laravel framework. This project can be run either with or without docker. This project using MySQL as default, you may custom it with editing `.env.example` or `.env.docker` file. 
## Run Project With Docker
##### 1. Preparation
Execute `chmod +x ./run-docker.sh` and `chmod +x ./stop-docker.sh`. It may requires `sudo`.
##### 2. Run Project
There is two options running this project with docker.
1. Just execute `.run-docker.sh`. It may take a while.
2. Execute these commands sequentially. Command above also contains these.
```
rm .env
cp .env.docker .env
docker-compose up -d
docker-compose exec app composer install
docker-compose exec app php artisan config:cache
docker-compose exec app php artisan migrate
```
##### 3. A Little test
Open your web browser then go to `http://localhost:8080`. It's a good sign when it show Laravel homepage. 
##### 4. Stop Docker
Execute `stop-docker.sh`.

## Run Project Without Docker
##### 1. Preparation
1. Create a database named `bareksa`. Or you can create your own custom named database, then edit `.env.example` file.
2. Execute `chmod +x ./run.sh`. It may requires `sudo`.
##### 2. Run Project
There is two options running this project without docker. 
1. Just execute `./run.sh`. It may take a while until artisan is serving.
2. Execute following command sequentially like a normal Laravel project. Command above also contains these.
```
rm .env
cp .env.example .env
composer install
php artisan config:cache
php artisan migrate
php artisan serve --port=8080
```
##### 3. A Little test
Open your web browser then go to `http://localhost:8080`. It's a good sign when it show Laravel homepage.
## Postman
I pushed my postman collection with the environment within this project in `.zip` format named `postman.zip`.

## Test
Run following code ```./vendor/bin/phpunit --testsuite Bareksa```

## Having trouble?
Let me know if you had any trouble with project installation either by email, phone, or whatsapp.
