<?php


namespace App\Repositories;


use App\Models\Topic;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

class TopicRepository
{
    protected $model;

    public function __construct(Topic $topic)
    {
        $this->model = $topic;
    }

    /**
     * Display paginated data.
     *
     * @param Request $request
     * @return mixed
     */
    public function paged(Request $request)
    {
        $limit = 10;
        $searchables = [
            'name', 'description'
        ];
        $data = $this->model->query();

        if ($request->has('limit')) {
            $limit = $request->input('limit');
        }

        if ($request->has('search')) {
            foreach ($searchables as $searchable) {
                $data->orWhereRaw('LOWER(' . $searchable . ') LIKE \'%' . strtolower($request->input('search')) . '%\'');
            }
        }

        return $data->paginate($limit);
    }

    /**
     * Get specified data of Model instance.
     *
     * @param $id
     * @return mixed
     */
    public function find($id) {
        return $this->model->find($id);
    }

    /**
     * Show rows count of data.
     *
     * @return mixed
     */
    public function count() {
        return $this->model->count();
    }

    /**
     * Store request to specified Model instance.
     *
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function create(Request $request)
    {
        return $this->model->create([
            'id' => Uuid::generate(4)->string,
            'name' => $request->name,
            'description' => $request->description
        ]);
    }

    /**
     * Update specified Model instance data.
     *
     * @param Topic $data
     * @param Request $request
     * @return Topic
     */
    public function update(Topic $data, Request $request)
    {
        $data->update([
            'name' => $request->name,
            'description' => $request->description
        ]);

        return $data;
    }

    /**
     * Remove specified data from Model instance.
     *
     * @param Topic $data
     * @throws \Exception
     */
    public function delete(Topic $data)
    {
        $data->delete();
    }
}
