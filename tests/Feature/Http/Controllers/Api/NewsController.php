<?php

namespace Tests\Feature\Http\Controllers\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Tests\TestCase;

class NewsController extends TestCase
{
    /**
     * @test
     */
    public function can_display_news_list()
    {
        $response = $this->json('GET', route('api::news.get'));

        $response
            ->assertJsonStructure([
                'meta' => [
                    'code',
                    'api_version',
                    'method',
                    'message'
                ],
                'page_info' => [
                    'total',
                    'per_page',
                    'first_page',
                    'current_page',
                    'last_page',
                    'first_page_url',
                    'last_page_url',
                    'next_page_url',
                    'prev_page_url',
                    'self_page_url',
                    'count',
                    'from',
                    'to'
                ],
                'errors',
                'data' => [
                    'message',
                    'item',
                    'items',
                    'additional'
                ]
            ])
            ->assertStatus(200);
    }

    /**
     * @test
     */
    public function can_create_a_news()
    {
        $topicResponse = $this->json('POST', route('api::topic.post'), [
            'name' => $name = Str::random(20),
            'description' => $description = Str::random(100),
        ]);

        $statuses = ['draft', 'publish', 'deleted'];

        $tags = [];
        $tagsCount = rand(1, 5);
        for($i=1; $i<=$tagsCount; $i++) {
            $tags[] = Str::random(10);
        }

        $response = $this->json('POST', route('api::news.post'), [
            'title' => $title = Str::random(20),
            'news_content' => $newsContent = Str::random(100),
            'status' => $status = $statuses[rand(0, 2)],
            'topic_id' => $topicId = $topicResponse['data']['item']['id'],
            'tags' => $tags
        ]);

        $response
            ->assertJsonStructure([
                'meta' => [
                    'code',
                    'api_version',
                    'method',
                    'message'
                ],
                'page_info' => [
                    'total',
                    'per_page',
                    'first_page',
                    'current_page',
                    'last_page',
                    'first_page_url',
                    'last_page_url',
                    'next_page_url',
                    'prev_page_url',
                    'self_page_url',
                    'count',
                    'from',
                    'to'
                ],
                'errors',
                'data' => [
                    'message',
                    'item' => [
                        'id',
                        'title',
                        'news_content',
                        'status',
                        'topic_id',
                        'topic_name',
                        'tags'
                    ],
                    'items',
                    'additional'
                ]
            ])
            ->assertJson([
                'data' => [
                    'item' => [
                        'title' => $title,
                        'news_content' => $newsContent,
                        'status' => $status,
                        'topic_id' => $topicId,
                        'topic_name' => $name,
                        'tags' => $tags
                    ]
                ]
            ])
            ->assertStatus(200);
    }

    /**
     * @test
     */
    public function can_show_a_news()
    {
        $topicResponse = $this->json('POST', route('api::topic.post'), [
            'name' => $name = Str::random(20),
            'description' => $description = Str::random(100),
        ]);

        $statuses = ['draft', 'publish', 'deleted'];

        $tags = [];
        $tagsCount = rand(1, 5);
        for($i=1; $i<=$tagsCount; $i++) {
            $tags[] = Str::random(10);
        }

        $newsResponse = $this->json('POST', route('api::news.post'), [
            'title' => $title = Str::random(20),
            'news_content' => $newsContent = Str::random(100),
            'status' => $status = $statuses[rand(0, 2)],
            'topic_id' => $topicId = $topicResponse['data']['item']['id'],
            'tags' => $tags
        ]);

        $response = $this->json('GET', route('api::news.show', [
            'id' => $newsResponse['data']['item']['id']
        ]));

        $response
            ->assertJsonStructure([
                'meta' => [
                    'code',
                    'api_version',
                    'method',
                    'message'
                ],
                'page_info' => [
                    'total',
                    'per_page',
                    'first_page',
                    'current_page',
                    'last_page',
                    'first_page_url',
                    'last_page_url',
                    'next_page_url',
                    'prev_page_url',
                    'self_page_url',
                    'count',
                    'from',
                    'to'
                ],
                'errors',
                'data' => [
                    'message',
                    'item' => [
                        'id',
                        'title',
                        'news_content',
                        'status',
                        'topic_id',
                        'topic_name',
                        'tags'
                    ],
                    'items',
                    'additional'
                ]
            ])
            ->assertJson([
                'data' => [
                    'item' => [
                        'title' => $title,
                        'news_content' => $newsContent,
                        'status' => $status,
                        'topic_id' => $topicId,
                        'topic_name' => $name,
                        'tags' => $tags
                    ]
                ]
            ])
            ->assertStatus(200);
    }

    /**
     * @test
     */
    public function can_edit_a_news()
    {
        $topicResponse = $this->json('POST', route('api::topic.post'), [
            'name' => $name = Str::random(20),
            'description' => $description = Str::random(100),
        ]);

        $statuses = ['draft', 'publish', 'deleted'];

        $tags = [];
        $tagsCount = rand(1, 5);
        for($i=1; $i<=$tagsCount; $i++) {
            $tags[] = Str::random(10);
        }

        $newsResponse = $this->json('POST', route('api::news.post'), [
            'title' => $title = Str::random(20),
            'news_content' => $newsContent = Str::random(100),
            'status' => $status = $statuses[rand(0, 2)],
            'topic_id' => $topicId = $topicResponse['data']['item']['id'],
            'tags' => $tags
        ]);

        $response = $this->json('PUT', route('api::news.put', [
            'id' => $newsResponse['data']['item']['id']
        ]), [
            'title' => $title = Str::random(20),
            'news_content' => $newsContent = Str::random(100),
            'status' => $status = $statuses[rand(0, 2)],
            'topic_id' => $topicId = $topicResponse['data']['item']['id'],
            'tags' => $tags
        ]);

        $response
            ->assertJsonStructure([
                'meta' => [
                    'code',
                    'api_version',
                    'method',
                    'message'
                ],
                'page_info' => [
                    'total',
                    'per_page',
                    'first_page',
                    'current_page',
                    'last_page',
                    'first_page_url',
                    'last_page_url',
                    'next_page_url',
                    'prev_page_url',
                    'self_page_url',
                    'count',
                    'from',
                    'to'
                ],
                'errors',
                'data' => [
                    'message',
                    'item' => [
                        'id',
                        'title',
                        'news_content',
                        'status',
                        'topic_id',
                        'topic_name',
                        'tags'
                    ],
                    'items',
                    'additional'
                ]
            ])
            ->assertJson([
                'data' => [
                    'item' => [
                        'title' => $title,
                        'news_content' => $newsContent,
                        'status' => $status,
                        'topic_id' => $topicId,
                        'topic_name' => $name,
                        'tags' => $tags
                    ]
                ]
            ])
            ->assertStatus(200);
    }

    /**
     * @test
     */
    public function can_delete_a_news()
    {
        $topicResponse = $this->json('POST', route('api::topic.post'), [
            'name' => $name = Str::random(20),
            'description' => $description = Str::random(100),
        ]);

        $statuses = ['draft', 'publish', 'deleted'];

        $tags = [];
        $tagsCount = rand(1, 5);
        for($i=1; $i<=$tagsCount; $i++) {
            $tags[] = Str::random(10);
        }

        $newsResponse = $this->json('POST', route('api::news.post'), [
            'title' => $title = Str::random(20),
            'news_content' => $newsContent = Str::random(100),
            'status' => $status = $statuses[rand(0, 2)],
            'topic_id' => $topicId = $topicResponse['data']['item']['id'],
            'tags' => $tags
        ]);

        $response = $this->json('DELETE', route('api::news.delete', [
            'id' => $newsResponse['data']['item']['id']
        ]));

        $response
            ->assertJsonStructure([
                'meta' => [
                    'code',
                    'api_version',
                    'method',
                    'message'
                ],
                'page_info' => [
                    'total',
                    'per_page',
                    'first_page',
                    'current_page',
                    'last_page',
                    'first_page_url',
                    'last_page_url',
                    'next_page_url',
                    'prev_page_url',
                    'self_page_url',
                    'count',
                    'from',
                    'to'
                ],
                'errors',
                'data' => [
                    'message',
                    'item' => [
                        'id',
                        'title',
                        'news_content',
                        'status',
                        'topic_id',
                        'topic_name',
                        'tags'
                    ],
                    'items',
                    'additional'
                ]
            ])
            ->assertJson([
                'data' => [
                    'item' => [
                        'title' => $title,
                        'news_content' => $newsContent,
                        'status' => $status,
                        'topic_id' => $topicId,
                        'topic_name' => $name
                    ]
                ]
            ])
            ->assertStatus(200);
    }
}
