<?php


namespace App\Repositories;


use App\Models\News;
use App\Models\NewsTag;
use App\Models\Topic;
use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;

class NewsRepository
{
    protected $model;

    public function __construct(News $topic)
    {
        $this->model = $topic;
    }

    /**
     * Display paginated data.
     *
     * @param Request $request
     * @return mixed
     */
    public function paged(Request $request)
    {
        $limit = 10;
        $searchables = [
            'title', 'content', 'topic_name'
        ];

        if ($request->has('withTrash')) {
            if ($request->has('withTrash') == true) {
                $data = $this->model->query();
            } else {
                $data = $this->model->query()->withoutTrash();
            }
        } else {
            $data = $this->model->query()->withoutTrash();
        }

        if ($request->has('status')) {
            $data->where('status', $request->input('status'));
        }

        if ($request->has('limit')) {
            $limit = $request->input('limit');
        }

        if ($request->has('search')) {
            foreach ($searchables as $searchable) {
                $data->orWhereRaw('LOWER(' . $searchable . ') LIKE \'%' . strtolower($request->input('search')) . '%\'');
            }
        }

        if ($request->has('topic')) {
            $data->where('topic_name', $request->input('topic'));
        }

        return $data->paginate($limit);
    }

    /**
     * Get specified data of Model instance.
     *
     * @param $id
     * @return mixed
     */
    public function find($id) {
        return $this->model->find($id);
    }

    /**
     * Show rows count of data.
     *
     * @return mixed
     */
    public function count() {
        return $this->model->count();
    }

    /**
     * Store request to specified Model instance.
     *
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function create(Request $request)
    {
        $data = $this->model->create([
            'id' => Uuid::generate(4)->string,
            'title' => $request->title,
            'content' => $request->news_content,
            'status' => $request->status,
            'topic_id' => $request->topic_id,
            'topic_name' => Topic::findOrFail($request->topic_id)->name
        ]);

        $tags = [];
        foreach ($request->tags as $tag) {
            $tags[] = new NewsTag([
                'id' => Uuid::generate(4)->string,
                'tag' => $tag
            ]);
        }
        $data->tags()->saveMany($tags);

        return $data;
    }

    /**
     * Update specified Model instance data.
     *
     * @param News $data
     * @param Request $request
     * @return News
     * @throws \Exception
     */
    public function update(News $data, Request $request)
    {
        $data->update([
            'title' => $request->title,
            'content' => $request->news_content,
            'status' => $request->status,
            'topic_id' => $request->topic_id,
            'topic_name' => Topic::findOrFail($request->topic_id)->name
        ]);

        $tags = [];
        foreach ($request->tags as $tag) {
            $tags[] = new NewsTag([
                'id' => Uuid::generate(4)->string,
                'tag' => $tag
            ]);
        }
        $data->tags()->delete();
        $data->tags()->saveMany($tags);

        return $data;
    }

    /**
     * Remove specified data from Model instance.
     *
     * @param News $data
     * @throws \Exception
     */
    public function delete(News $data)
    {
        $data->update([
            'status' => 'deleted'
        ]);
    }
}
