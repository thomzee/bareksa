<?php

namespace Tests\Feature\Http\Controllers\Api;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Tests\TestCase;

class TopicController extends TestCase
{
    /**
     * @test
     */
    public function can_display_topic_list()
    {
        $response = $this->json('GET', route('api::topic.get'));

        $response
            ->assertJsonStructure([
                'meta' => [
                    'code',
                    'api_version',
                    'method',
                    'message'
                ],
                'page_info' => [
                    'total',
                    'per_page',
                    'first_page',
                    'current_page',
                    'last_page',
                    'first_page_url',
                    'last_page_url',
                    'next_page_url',
                    'prev_page_url',
                    'self_page_url',
                    'count',
                    'from',
                    'to'
                ],
                'errors',
                'data' => [
                    'message',
                    'item',
                    'items',
                    'additional'
                ]
            ])
            ->assertStatus(200);
    }

    /**
     * @test
     */
    public function can_create_a_topic()
    {
        $response = $this->json('POST', route('api::topic.post'), [
            'name' => $name = Str::random(20),
            'description' => $description = Str::random(100),
        ]);

        $response
            ->assertJsonStructure([
                'meta' => [
                    'code',
                    'api_version',
                    'method',
                    'message'
                ],
                'page_info' => [
                    'total',
                    'per_page',
                    'first_page',
                    'current_page',
                    'last_page',
                    'first_page_url',
                    'last_page_url',
                    'next_page_url',
                    'prev_page_url',
                    'self_page_url',
                    'count',
                    'from',
                    'to'
                ],
                'errors',
                'data' => [
                    'message',
                    'item' => [
                        'id',
                        'name',
                        'description',
                    ],
                    'items',
                    'additional'
                ]
            ])
            ->assertJson([
                'data' => [
                    'item' => [
                        'name' => $name,
                        'description' => $description
                    ]
                ]
            ])
            ->assertStatus(200);

        $this->assertDatabaseHas('topics', [
            'name' => $name,
            'description' => $description
        ]);
    }

    /**
     * @test
     */
    public function can_show_a_topic()
    {
        $response = $this->json('POST', route('api::topic.post'), [
            'name' => $name = Str::random(20),
            'description' => $description = Str::random(100),
        ]);

        $response = $this->json('GET', route('api::topic.show', [
            'id' => $response['data']['item']['id']
        ]));

        $response
            ->assertJsonStructure([
                'meta' => [
                    'code',
                    'api_version',
                    'method',
                    'message'
                ],
                'page_info' => [
                    'total',
                    'per_page',
                    'first_page',
                    'current_page',
                    'last_page',
                    'first_page_url',
                    'last_page_url',
                    'next_page_url',
                    'prev_page_url',
                    'self_page_url',
                    'count',
                    'from',
                    'to'
                ],
                'errors',
                'data' => [
                    'message',
                    'item' => [
                        'id',
                        'name',
                        'description',
                    ],
                    'items',
                    'additional'
                ]
            ])
            ->assertJson([
                'data' => [
                    'item' => [
                        'name' => $response['data']['item']['name'],
                        'description' => $response['data']['item']['description']
                    ]
                ]
            ])
            ->assertStatus(200);
    }

    /**
     * @test
     */
    public function can_edit_a_topic()
    {
        $response = $this->json('POST', route('api::topic.post'), [
            'name' => $name = Str::random(20),
            'description' => $description = Str::random(100),
        ]);

        $response = $this->json('PUT', route('api::topic.put', [
            'id' => $response['data']['item']['id']
        ]), [
            'name' => $name = Str::random(20),
            'description' => $description = Str::random(100),
        ]);

        $response
            ->assertJsonStructure([
                'meta' => [
                    'code',
                    'api_version',
                    'method',
                    'message'
                ],
                'page_info' => [
                    'total',
                    'per_page',
                    'first_page',
                    'current_page',
                    'last_page',
                    'first_page_url',
                    'last_page_url',
                    'next_page_url',
                    'prev_page_url',
                    'self_page_url',
                    'count',
                    'from',
                    'to'
                ],
                'errors',
                'data' => [
                    'message',
                    'item' => [
                        'id',
                        'name',
                        'description',
                    ],
                    'items',
                    'additional'
                ]
            ])
            ->assertStatus(200);
        $this->assertDatabaseHas('topics', [
            'name' => $name,
            'description' => $description
        ]);
    }

    /**
     * @test
     */
    public function can_delete_a_topic()
    {
        $response = $this->json('POST', route('api::topic.post'), [
            'name' => $name = Str::random(20),
            'description' => $description = Str::random(100),
        ]);

        $response = $this->json('DELETE', route('api::topic.delete', [
            'id' => $response['data']['item']['id']
        ]));

        $response
            ->assertJsonStructure([
                'meta' => [
                    'code',
                    'api_version',
                    'method',
                    'message'
                ],
                'page_info' => [
                    'total',
                    'per_page',
                    'first_page',
                    'current_page',
                    'last_page',
                    'first_page_url',
                    'last_page_url',
                    'next_page_url',
                    'prev_page_url',
                    'self_page_url',
                    'count',
                    'from',
                    'to'
                ],
                'errors',
                'data' => [
                    'message',
                    'item' => [
                        'id',
                        'name',
                        'description',
                    ],
                    'items',
                    'additional'
                ]
            ])
            ->assertJson([
                'data' => [
                    'item' => [
                        'name' => $response['data']['item']['name'],
                        'description' => $response['data']['item']['description']
                    ]
                ]
            ])
            ->assertStatus(200);
    }
}
