<?php


namespace App\Mappers;


use App\Services\Mapper\BaseMapper;
use App\Services\Mapper\MapperContract;

class TopicMapper extends BaseMapper implements MapperContract
{
    /**
     * Map single object to desired result.
     *
     * @param $item
     * @return array|mixed
     */
    function single($item)
    {
        return [
            "id" => $item->id,
            "name" => $item->name,
            "description" => $item->description
        ];
    }
}
