#!/usr/bin/env bash
rm .env
cp .env.example .env
composer install
php artisan config:cache
php artisan migrate
php artisan serve --port=8080
