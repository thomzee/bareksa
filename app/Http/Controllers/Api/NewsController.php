<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\News\CreateRequest;
use App\Http\Requests\Api\News\UpdateRequest;
use App\Mappers\NewsMapper;
use App\Repositories\NewsRepository;
use App\Services\Mapper\Facades\Mapper;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class NewsController extends Controller
{
    protected $news;

    public function __construct(NewsRepository $news)
    {
        $this->news = $news;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        try {
            $data = $this->news->paged($request);
            return Mapper::list(new NewsMapper(), $data, $this->news->count(), $request->method());
        } catch (\Exception $e) {
            return Mapper::error($e->getMessage(), $request->method());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateRequest $request
     * @return Response
     */
    public function store(CreateRequest $request)
    {
        DB::beginTransaction();
        try {
            $data = $this->news->create($request);
            DB::commit();
            return Mapper::single(new NewsMapper(), $data, $request->method());
        } catch (\Exception $e) {
            DB::rollBack();
            return Mapper::error($e->getMessage(), $request->method());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function show(Request $request, $id)
    {
        try {
            $data = $this->news->find($id);
            return Mapper::single(new NewsMapper(), $data, $request->method());
        } catch (\Exception $e) {
            return Mapper::error($e->getMessage(), $request->method());
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param int $id
     * @return Response
     */
    public function update(UpdateRequest $request, $id)
    {
        DB::beginTransaction();
        try {
            $data = $this->news->find($id);
            $data = $this->news->update($data, $request);
            DB::commit();
            return Mapper::single(new NewsMapper(), $data, $request->method());
        } catch (\Exception $e) {
            DB::rollBack();
            return Mapper::error($e->getMessage(), $request->method());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function destroy(Request $request, $id)
    {
        DB::beginTransaction();
        try {
            $data = $this->news->find($id);
            $this->news->delete($data);
            DB::commit();
            return Mapper::single(new NewsMapper(), $data, $request->method());
        } catch (\Exception $e) {
            DB::rollBack();
            return Mapper::error($e->getMessage(), $request->method());
        }
    }
}
