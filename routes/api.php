<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['as' => 'api::', 'namespace' => 'Api', 'prefix' => 'v1'], function () {
    Route::group(['as' => 'topic.', 'prefix' => 'topic'], function () {
        Route::get('/', ['as' => 'get', 'uses' => 'TopicController@index']);
        Route::post('/create', ['as' => 'post', 'uses' => 'TopicController@store']);
        Route::put('/edit/{id}', ['as' => 'put', 'uses' => 'TopicController@update']);
        Route::get('/show/{id}', ['as' => 'show', 'uses' => 'TopicController@show']);
        Route::delete('/delete/{id}', ['as' => 'delete', 'uses' => 'TopicController@destroy']);
    });

    Route::group(['as' => 'news.', 'prefix' => 'news'], function () {
        Route::get('/', ['as' => 'get', 'uses' => 'NewsController@index']);
        Route::post('/create', ['as' => 'post', 'uses' => 'NewsController@store']);
        Route::put('/edit/{id}', ['as' => 'put', 'uses' => 'NewsController@update']);
        Route::get('/show/{id}', ['as' => 'show', 'uses' => 'NewsController@show']);
        Route::delete('/delete/{id}', ['as' => 'delete', 'uses' => 'NewsController@destroy']);
    });
});
