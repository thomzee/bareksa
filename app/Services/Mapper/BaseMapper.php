<?php


namespace App\Services\Mapper;


/**
 * Class BaseMapper
 * @package App\Services\Mapper
 */
abstract class BaseMapper
{
    /**
     * Loop through single() function to generate multiple mapped data.
     *
     * @param $items
     * @return array
     */
    public function list($items)
    {
        $result = [];
        foreach ($items as $item) {
            $result[] = $this->single($item);
        }
        return $result;
    }
}
