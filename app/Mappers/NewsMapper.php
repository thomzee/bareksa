<?php


namespace App\Mappers;


use App\Services\Mapper\BaseMapper;
use App\Services\Mapper\MapperContract;

class NewsMapper extends BaseMapper implements MapperContract
{
    /**
     * Map single object to desired result.
     *
     * @param $item
     * @return array|mixed
     */
    function single($item)
    {
        return [
            "id" => $item->id,
            "title" => $item->title,
            "news_content" => $item->content,
            "status" => $item->status,
            "topic_id" => $item->topic_id,
            "topic_name" => $item->topic_name,
            "tags" => $item->tags()->pluck('tag'),
        ];
    }
}
